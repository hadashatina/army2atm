unit Unit1;

interface

uses
  ShellApi,FileUtil ,ComObj,Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Mask, ToolEdit, RXDBCtrl, Spin,  Db, Registry,
  DBTables, Placemnt, Menus, Scale250;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Cb_Alias_Atm: TComboBox;
    Edit5: TEdit;
    Edit6: TEdit;
    Label5: TLabel;
    Label9: TLabel;
    Label7: TLabel;
    Button2: TButton;
    Memo1: TMemo;
    Database1: TDatabase;
    Query1: TQuery;
    Table1: TTable;
    Label2: TLabel;
    Edit1: TEdit;
    Button3: TButton;
    Edit2: TEdit;
    Label4: TLabel;
    Label6: TLabel;
    RadioGroup1: TRadioGroup;
    Label8: TLabel;
    Edit3: TEdit;
    Qry_Tmp: TQuery;
    Query2: TQuery;
    Query3: TQuery;
    Label10: TLabel;
    Edit4: TEdit;
    Label11: TLabel;
    Spe_DaysForPiraon: TSpinEdit;
    FormStorage1: TFormStorage;
    Tbl_Army_Paradox: TQuery;
    AsmactaList: TListBox;
    Scale1: TScale;
    Edt_InvoicDateBeg: TDateEdit;
    Edt_InvoicDateEnd: TDateEdit;
    Label1: TLabel;
    Label3: TLabel;
    Edt_InvoicBeg: TEdit;
    Edt_InvoicEnd: TEdit;
    Bevel1: TBevel;
    Label12: TLabel;
    Cb_Alias_Army: TComboBox;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Memo1Click(Sender: TObject);
    procedure Database1Login(Database: TDatabase; LoginParams: TStrings);
    procedure Button3Click(Sender: TObject);
    procedure ImportFileToMakav(Sender: TObject);
    procedure AsmactaListDblClick(Sender: TObject);
    procedure Database1BeforeConnect(Sender: TObject);
    procedure Tbl_Army_ParadoxBeforeOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    procedure ReadValue (Key : String);
    procedure WriteValue (Key : String);
    procedure Log(TmpStr:String);
    Function CheckHesbonitNumber (HesbonitNumber , YehusYear: Integer): Boolean;
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses AtmConst;



{$R *.DFM}
procedure TForm1.WriteValue (Key : String);
var
   Reg : TRegistry;
   i : integer;
begin
    Reg:=TRegistry.Create;
    Try
       Reg.RootKey:=RegRootForAtm;
       Reg.OpenKey(RegKeyForAtm+key,True);
       for i:=0 to  ComponentCount -1 do
       begin
                if  Components[i] is TSpinEdit Then
                   Reg.WriteString (( Components[i] As TSpinEdit).Name,( Components[i] As TSpinEdit).Text)
                else
                  if  Components[i] is TCustomEdit Then
                     Reg.WriteString (( Components[i] As TCustomEdit).Name,( Components[i] As TCustomEdit).Text)
                  else
                    if  Components[i] is TComboBox Then
                       Reg.WriteString (( Components[i] As TComboBox).Name,( Components[i] As TComboBox).Text);
        end;
     Finally
            Reg.Free;
     End;
end;

procedure TForm1.ReadValue (Key : String);
var
   Reg : TRegistry;
   i : integer;
begin
    Reg:=TRegistry.Create;
    Try
       Reg.RootKey:=RegRootForAtm;
       if Reg.KeyExists(RegKeyForAtm+key) Then
       Begin
           Reg.OpenKey(RegKeyForAtm+key,False);
           for i:=0 to  ComponentCount -1 do
           begin
                  if  Components[i] is TSpinEdit Then
                  begin
                     if Reg.ValueExists(( Components[i] As TSpinEdit).Name) Then
                         ( Components[i] As TSpinEdit).Text := Reg.ReadString (( Components[i] As TSpinEdit).Name)
                  end
                  else
                    if  Components[i] is TCustomEdit Then
                    begin
                       if Reg.ValueExists(( Components[i] As TCustomEdit).Name) Then
                           ( Components[i] As TCustomEdit).Text := Reg.ReadString (( Components[i] As TCustomEdit).Name);
                    end
                    else
                      if  Components[i] is TComboBox Then
                      begin
                         if Reg.ValueExists(( Components[i] As TComboBox).Name) Then
                             ( Components[i] As TComboBox).Text := Reg.ReadString (( Components[i] As TComboBox).Name);
                      end
            end;
       End;
     Finally
            Reg.Free;
     End;
end;
Function TForm1.CheckHesbonitNumber (HesbonitNumber , YehusYear: Integer): Boolean;
begin
   Qry_Tmp.Close;
   Qry_Tmp.Sql.Clear;
   Qry_Tmp.Sql.Add('select 1 As A from Makav where HesbonitKind in (1,3) and YehusYear='+IntToStr (YehusYear)+' And HesbonitNumber ='+IntToStr (HesbonitNumber));
   Qry_Tmp.Open;
   Result := Qry_Tmp.Fields[0].IsNull;
   Qry_Tmp.Close;
end;

procedure TForm1.Log(TmpStr:String);
begin
   if TmpStr <> '' Then
      Memo1.Lines.Add (FormatDateTime ('HH:mm:ss',now)+' - '+TmpStr)
   else
      Memo1.Lines.Add ('');
   Application.ProcessMessages;
   Form1.Refresh;
   Application.ProcessMessages;
end;
procedure TForm1.Button1Click(Sender: TObject);
Var
   TmpStr: String;
   Tf : TextFile;
begin
    Memo1.Lines.Clear;
    ImportFileToMakav (Sender);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
   ShortDateFormat := 'DD/MM/YYYY';
   DateSeparator := '/';
   ReadValue ('\AtmImportFromExcel');
   Session.GetAliasNames (Cb_Alias_Atm.Items);
   Session.GetAliasNames (Cb_Alias_Army.Items);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
   deletefile (Gettempdir+'Log.txt');
   Memo1.Lines.Clear;
   WriteValue ('\AtmImportFromExcel');
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
   Close;
end;

procedure TForm1.Memo1Click(Sender: TObject);
begin
   Memo1.Lines.SaveToFile (Gettempdir+'Log.txt');
   ShellExecute(Application.Handle,'open',PChar('notepad.exe'),PChar(GetTempDir+'Log.txt'),nil,SW_MAXIMIZE);

end;

procedure TForm1.Database1Login(Database: TDatabase;
  LoginParams: TStrings);
begin
   LoginParams.Values['USER NAME'] := Edit5.Text;
   LoginParams.Values['PASSWORD'] := Edit6.Text;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
   Memo1.Lines.Clear;
end;

procedure TForm1.ImportFileToMakav(Sender: TObject);
Var
   HesbonitShibNoList : TStringList;
   TnuaNoFoundList : TStringList;
   HaveTnua : Boolean;
begin
   HesbonitShibNoList := TStringList.Create;
   TnuaNoFoundList := TStringList.Create;
   Log('�����');
   Database1.Connected := False;
   Database1.Connected := True;
   Log('����� ����');
   Table1.TableName := 'Makav';
   Table1.IndexFieldNames := 'HesbonitKind;YehusYear;HesbonitNumber';
   Table1.Open;
   Log('���� ���� ����');
   try Database1.Execute ('drop trigger Dont_Update_Shib'); except end;
   try Database1.Execute ('drop trigger Dont_Update_Shib1'); except end;
//ProRikuz
   Tbl_Army_Paradox.ParamByName ('PInvoiceNoBeg').AsInteger := StrToInt(Edt_InvoicBeg.Text);
   Tbl_Army_Paradox.ParamByName ('PInvoiceNoEnd').AsInteger := StrToInt(Edt_InvoicEnd.Text);
   Tbl_Army_Paradox.ParamByName ('PDateBeg').AsDateTime := Edt_InvoicDateBeg.Date;
   Tbl_Army_Paradox.ParamByName ('PDateEnd').AsDateTime := Edt_InvoicDateEnd.Date;
   Tbl_Army_Paradox.Sql.SaveToFile (getTempDir+'Qry.SQl' );
   Tbl_Army_Paradox.Open;
   while not Tbl_Army_Paradox.Eof do
   begin
     HaveTnua := false;
     if (Tbl_Army_Paradox.FieldByName ('SendNo').AsString <> '')And(Tbl_Army_Paradox.FieldByName ('SendNo').AsInteger > 0) Then
     begin
        Query1.Sql.Clear;
        Query1.Sql.Add ('select Shib.ShibNo From Atmshib shib Where  Lakno1 = '+Edit1.Text+' And ShibKind > -1');
        Query1.Sql.Add ('And  CarNum = '+QuotedStr(Tbl_Army_Paradox.FieldByName ('RishuiNo').AsString)+' And ShibTuda = '+Tbl_Army_Paradox.FieldByName ('SendNo').AsString);
//        Query1.Sql.SaveToFile('c:\tony.sql');
        Query1.Open;
        HaveTnua := Query1.FieldByName ('ShibNo').AsInteger > 0;
//        ShowMessage ('Shibno - ' + Query1.FieldByName ('ShibNo').AsString);
        Query1.Close;
     end;

     if HaveTnua Then
     begin
       try

        Database1.StartTransaction;
        if Table1.FindKey ([1,Tbl_Army_Paradox.FieldByName ('InVoiceYear').AsInteger,Tbl_Army_Paradox.FieldByName ('InvoiceNo').AsInteger]) Then
        begin
           Table1.Edit;
           log('����� ������� ���� - '+Tbl_Army_Paradox.FieldByName ('InvoiceNo').AsString);
        end
        else
        begin
          Table1.Insert;
          log('������� ���� ���� - '+Tbl_Army_Paradox.FieldByName ('InvoiceNo').AsString);
        end;
           if Tbl_Army_Paradox.FieldByName ('MAM').AsInteger = 0 Then
               Table1.FieldByName ('HesbonitKind').AsInteger :=hkLloMam
           else
               Table1.FieldByName ('HesbonitKind').AsInteger :=hkShekel;
               Table1.FieldByName ('Total_Zikuy').AsInteger :=0;
               Table1.FieldByName ('YehusYear').AsInteger :=Tbl_Army_Paradox.FieldByName ('InVoiceYear').AsInteger;
               Table1.FieldByName ('YehusMonth').AsInteger :=Tbl_Army_Paradox.FieldByName ('InVoiceMonth').AsInteger;
               Table1.FieldByName ('HesbonitNumber').AsInteger :=Tbl_Army_Paradox.FieldByName ('InvoiceNo').AsInteger;
               Table1.FieldByName ('CodeLakoach').AsString :=Edit1.Text;
               Table1.FieldByName ('LakoachGroup').AsString :=Edit4.Text;
               Table1.FieldByName ('HesbonitDate').AsDateTime := Tbl_Army_Paradox.FieldByName ('InVoiceDate').AsDateTime;
               Table1.FieldByName ('TotalSumFromTnua').AsFloat := Tbl_Army_Paradox.FieldByName ('Price').AsFloat;
               Table1.FieldByName ('DatePiraon').AsDatetime := Table1.FieldByName ('HesbonitDate').AsDateTime + Spe_DaysForPiraon.Value ;
               Table1.FieldByName ('MamHesbonit').AsFloat := Tbl_Army_Paradox.FieldByName ('MAM').AsFloat;
               Table1.FieldByName ('TotalHesbonitWithMam').AsFloat := Tbl_Army_Paradox.FieldByName ('AllPrice').AsFloat;
               Table1.FieldByName ('MamPercent').AsFloat := Trunc(Tbl_Army_Paradox.FieldByName ('MAM').AsFloat / Tbl_Army_Paradox.FieldByName ('Price').AsFloat*100);
               Table1.FieldByName ('MamRound').AsInteger :=0;
               Table1.FieldByName ('DateHafakatHeshbonit').AsDateTime := Trunc(Now) ;
               Table1.FieldByName ('Status').AsInteger :=0;
               Table1.FieldByName ('CodeHaavaraToHan').AsInteger :=RadioGroup1.ItemIndex;
               if length (Edit2.Text) > 0 Then
                  Table1.FieldByName ('PratimToHan').AsString :=Edit2.Text
               else
                  Table1.FieldByName ('PratimToHan').Clear;
               if length (Edit3.Text) > 0 Then
                  Table1.FieldByName ('HanForLak').AsString :=Edit3.Text
               else
                  Table1.FieldByName ('HanForLak').Clear;
               Table1.Post;
                 {����� ������}
                 Query1.Sql.Clear;
                 Query1.Sql.Add ('select Shib.ShibNo From Atmshib shib Where  Lakno1 = '+Edit1.Text+' And ShibKind > -1');
                 Query1.Sql.Add ('And  CarNum = '+QuotedStr (Tbl_Army_Paradox.FieldByName ('RishuiNo').AsString)+' And ShibTuda = '+Tbl_Army_Paradox.FieldByName ('SendNo').AsString);
                 Query1.Open;
                 HesbonitShibNoList.Clear;
                 while not Query1.Eof do
                 begin
                    if HesbonitShibNoList.Count >0  Then
                      HesbonitShibNoList.Add (','+Query1.Fields[0].AsString)
                    else
                      HesbonitShibNoList.Add (Query1.Fields[0].AsString);
                    Query1.Next;
                 end;
                 if HesbonitShibNoList.Count > 0 then
                 begin
                    Query1.Close;
                    Query1.Sql.Clear;
                    Query1.Sql.Add ('update shib ');
                    Query1.Sql.Add ('Set  Yemida1 =1 , Quntity1 =1 , LakCodeBizua = 1 , LakHesbonit = '+Table1.FieldByName ('HesbonitNumber').AsString+' , LakHesbonitDate = '+QuotedStr (FormatDatetime ('mm/dd/yyyy',Table1.FieldByName ('HesbonitDate').AsDateTime)));
                    Query1.Sql.Add (' ,Price1 = '+Tbl_Army_Paradox.FieldByName ('Price').AsString+' / '+IntToStr(HesbonitShibNoList.Count)+', PriceQuntity1 = '+Tbl_Army_Paradox.FieldByName ('Price').AsString+' / '+IntToStr(HesbonitShibNoList.Count));
                    Query1.Sql.Add (' ,Price2 = 0.85 * '+Tbl_Army_Paradox.FieldByName ('Price').AsString+' / '+IntToStr(HesbonitShibNoList.Count)+', PriceQuntity2 = 0.85 * '+Tbl_Army_Paradox.FieldByName ('Price').AsString+' / '+IntToStr(HesbonitShibNoList.Count));
                    Query1.Sql.Add (' ,Price3 = '+Tbl_Army_Paradox.FieldByName ('Price').AsString+' / '+IntToStr(HesbonitShibNoList.Count)+', PriceQuntity3 = '+Tbl_Army_Paradox.FieldByName ('Price').AsString+' / '+IntToStr(HesbonitShibNoList.Count));
                    Query1.Sql.Add (' ,ShibTotalKm = '+Tbl_Army_Paradox.FieldByName ('Distance').AsString+' / '+IntToStr(HesbonitShibNoList.Count));
                    Query1.Sql.Add ('From Atmshib shib');
                    Query1.Sql.Add ('where ShibKind > -1 And ShibNo in ('+HesbonitShibNoList.Text+')');
                    Application.ProcessMessages;
                    try
                       Query1.ExecSQL;
                       Log (' ������ '+IntToStr(Query1.RowsAffected)+' ����� '+ StringReplace (StringReplace (Query1.Sql.Text,chr(13),'',[rfreplaceall]),chr(10),'',[rfReplaceAll]));

                    except on e:exception do  Log ('����� - '+e.message);end;
                 end;
                 if HesbonitShibNoList.Count < 1 then
                 begin
                    Log ('������� ���� - '+Tbl_Army_Paradox.FieldByName ('InvoiceNo').AsString+' ������  - '+Tbl_Army_Paradox.FieldByName ('OrderNo').AsString+ ' �� ���  '+Tbl_Army_Paradox.FieldByName ('RishuiNo').AsString+'  �� �����. ');
                    TnuaNoFoundList.Add ('������� ���� - '+Tbl_Army_Paradox.FieldByName ('InvoiceNo').AsString+' ������  - '+Tbl_Army_Paradox.FieldByName ('OrderNo').AsString+ ' �� ���  '+Tbl_Army_Paradox.FieldByName ('Mis_Rishui').AsString+'  �� �����. ');
                 end;
               Database1.Commit;
               except on e:exception do
               begin
                   Log('���� ������ ������� -  '+Tbl_Army_Paradox.FieldByName ('InvoiceNo').AsString +' ����� ����� -  ' +e.message);
                   if Database1.InTransaction Then
                      Database1.Rollback;
               end;
              end;{Try}
           end
     else
     begin

          Log ('������� ���� - '+Tbl_Army_Paradox.FieldByName ('InvoiceNo').AsString+' �����  - '+Tbl_Army_Paradox.FieldByName ('SendNo').AsString+ ' �� ���  '+Tbl_Army_Paradox.FieldByName ('RishuiNo').AsString+' ������ -  '+ Tbl_Army_Paradox.FieldByName ('BeThereDate').AsString+'  �� �����. ');
          TnuaNoFoundList.Add ('������� ���� - '+Tbl_Army_Paradox.FieldByName ('InvoiceNo').AsString+' �����  - '+Tbl_Army_Paradox.FieldByName ('SendNo').AsString+ ' �� ���  '+Tbl_Army_Paradox.FieldByName ('RishuiNo').AsString+' ������ -  '+Tbl_Army_Paradox.FieldByName ('BeThereDate').AsString +'  �� �����. ');
     end;
     Tbl_Army_Paradox.next;
   end;{while Tbl_Army_Paradox.Eof}
   Tbl_Army_Paradox.Close;
   Table1.Close;
   Database1.Connected := False;

   if TnuaNoFoundList.Count > 0 then
   begin
      Memo1.Lines.Add('');
      Memo1.Lines.Add('/*********           ��"� �������� ��� �����           *********/');
      Memo1.Lines.Add('/*********           ����� �����:'+FormatDateTime ('DD/MM/YYYY',now)+'              *********/');
      Memo1.Lines.Add('');
      Memo1.Lines.AddStrings (TnuaNoFoundList);
   end;
   AsmactaList.Items.AddStrings (TnuaNoFoundList);
   TnuaNoFoundList.Free;
   HesbonitShibNoList.Free;
   Memo1Click(sender);
end;

procedure TForm1.AsmactaListDblClick(Sender: TObject);
begin
   AsmactaList.Items.SaveToFile (getTempDir+'NotFound.txt');
   ShellExecute(Application.Handle,'open',PChar('notepad.exe'),PChar(GetTempDir+'NotFound.txt'),nil,SW_MAXIMIZE);

end;

procedure TForm1.Database1BeforeConnect(Sender: TObject);
begin
    Database1.AliasName := Cb_Alias_Atm.Text;
    Application.Title := 'Alias - '+Database1.AliasName;
end;

procedure TForm1.Tbl_Army_ParadoxBeforeOpen(DataSet: TDataSet);
begin
   Tbl_Army_Paradox.DatabaseName := Cb_Alias_Army.Text;
end;

end.

